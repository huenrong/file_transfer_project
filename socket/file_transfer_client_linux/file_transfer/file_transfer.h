/**
 * 文件名称: file_transfer.h
 * 摘    要: 与文件传输相关的操作函数头文件
 * 来    源: huenrong
 *
 * 当前版本: 1.0 
 * 作    者: huenrong
 * 完成日期: 2019-07-28
 **/


#ifndef __FILE_TRANSFER_H
#define __FILE_TRANSFER_H

#ifdef __cplusplus
extern "C" {
#endif


#define     CLIENT_HEAD             0xAA        // 客户端发送的消息头
#define     SERVER_HEAD             0xBB        // 服务器发送的消息头
#define     MAX_USER_DATA_LEN       1024        // 传输用户数据最大长度(不包括CRC)
#define     MAX_FILE_NAME_LEN       50          // 支持的最大文件名长度


// 定义传输数据类型
enum transfer_data_type
{
    TRANSFER_FILE_NAME = 0xA0,      // 传输文件名
    TRANSFER_FILE_SIZE = 0xA1,      // 传输文件大小
    TRANSFER_FILE_DATA = 0xA2,      // 传输文件内容
    TRANSFER_FILE_END = 0xA3        // 传输文件结束
};

// 传输文件内容应答类型
enum transfer_response
{
    RECV_OK = 0x00,             // 接收成功
    HEAD_ERR = 0x01,            // 帧头错误
    CRC_ERR = 0x02,             // CRC错误
    TRANSFER_OK = 0x03,         // 文件传输成功
    TRANSFER_FAIL = 0x04,       // 文件传输失败
    OTHER_ERR = 0x0A            // 其它错误
};

#pragma pack(1)
// 传输协议结构体
struct file_transfer
{
    unsigned char frame_head;       // 帧头
    unsigned char frame_number;     // 帧序号
    unsigned char user_data_type;   // 用户数据类型(详见enum transfer_data_type)
    unsigned short user_data_len;   // 用户数据长度
    unsigned char user_data[MAX_USER_DATA_LEN + 2];     // 用户数据(CRC附加到数据末2字节)
};

// 记录文件信息
struct file_info
{
    unsigned char file_name[MAX_FILE_NAME_LEN];     // 文件名
    unsigned long file_size;        // 文件大小
};

#pragma pack()

/************************************************************************
函数名称: get_file_size
函数功能: 获取文件大小
函数参数: path: 文件路径
函数返回: 成功: 返回文件大小
          失败: 返回-1
************************************************************************/
unsigned long get_file_size(const char *path);

/************************************************************************
函数名称: file_transfer_client_function
函数功能: 文件传输客户端功能函数
函数参数: client_fd: 客户端套接字
函数返回: 成功: 返回0
          失败: 返回-1/对应错误码
************************************************************************/
int file_transfer_client_function(const int client_fd);

#ifdef __cplusplus
}
#endif

#endif /* __FILE_TRANSFER_H */
