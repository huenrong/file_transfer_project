/**
 * 文件名称: socket.h
 * 摘    要: 与socket相关操作头文件
 * 来    源: 自我总结
 *
 * 当前版本: 1.0 
 * 作    者: huenrong
 * 完成日期: 2019-07-07
 **/


#ifndef __SOCKET_H
#define __SOCKET_H

#ifdef __cplusplus
extern "C" {
#endif


/************************************************************************
函数名称: creat_tcp_server
函数功能: 创建tcp server线程
函数参数: tcp_server_function: tcp server的功能函数
          tcp_server_port: tcp server 监听串口号
函数返回: 成功: 返回0
          失败: 返回-1
************************************************************************/
int creat_tcp_server(int (* tcp_server_function)(const int *), const int tcp_server_port);


#ifdef __cplusplus
}
#endif

#endif /* __SOCKET_H */

