/**
 * 文件名称: maic.c
 * 摘    要: 文件传输服务器入口代码
 * 来    源: 自我总结
 *
 * 当前版本: 1.0 
 * 作    者: huenrong
 * 完成日期: 2019-07-07
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>

#include "./socket/socket.h"
#include "./file_transfer/file_transfer.h"
#include "./write_log/write_log.h"


int main(int argc, char *argv[])
{
    unsigned int server_port = 0;        // 服务器port

    if (2 != argc)
	{
		printf("usage : %s <port>\r\n", argv[0]);
		
        return -1;
	}
    
    server_port = atoi(argv[1]);

    // 程序启动打印/记录日志(一般信息)
#ifdef DEBUG
    printf("file_transfer_server start\n");
#endif
    WRITE_LOG_FILE(LOG_INFO, "file_transfer_server start");
    

    // 创建文件传输服务器，同时传递server功能函数和监听端口
    creat_tcp_server(file_transfer_server_function, server_port);

    while (1);

    return 0;
}
