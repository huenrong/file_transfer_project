/**
 * 文件名称: write_log.c
 * 摘    要: linux应用程序下记录日志源文件
 * 来    源: 参考: https://blog.csdn.net/qq_39436605/article/details/81181061
 *
 * 当前版本: 1.0
 * 作    者: huenrong
 * 完成日期: 2019年06月20日
 **/

#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

#include "write_log.h"


/************************************************************************
函数名称: get_time
函数功能: 获取时间串(时间串样式: YYYY-MM-DD HH:MIN:SS.Usec)
函数参数: get_time_str: 获取到的时间串
          flag: 0: 时间串样式: [YYYY-MM-DD HH:MIN:SS.Usec]
                1: 时间串样式: YYYY-MM-DD_HH-MIN-SS-Usec
函数返回: 无
************************************************************************/
static void get_time(unsigned char *get_time_str, const unsigned char flag)
{
    struct tm sys_time = {0};
    time_t current_time = {0};
    struct timeval get_time_value = {0};

    unsigned char microsecond[20] = {0};        // 微秒
    unsigned char millisecond[20] = {0};        // 毫秒

    if (NULL == get_time_str)
    {
        return;
    }

    current_time = time(NULL);
    localtime_r(&current_time, &sys_time);      // localtime_r是线程安全的

    gettimeofday(&get_time_value, NULL);
    sprintf(microsecond, "%ld", get_time_value.tv_usec);       // 获取微秒
    strncpy(millisecond, microsecond, 3);       // 微秒的前3位为毫秒(1毫秒=1000微秒)

    if (0 == flag)
    {
        sprintf(get_time_str, "[%04d-%02d-%02d %02d:%02d:%02d.%3.3s]",
                sys_time.tm_year+1900, sys_time.tm_mon+1, sys_time.tm_mday,
                sys_time.tm_hour, sys_time.tm_min, sys_time.tm_sec, millisecond);
    }
    else if (1 == flag)
    {
        sprintf(get_time_str, "%04d-%02d-%02d_%02d-%02d-%02d-%3.3s",
                sys_time.tm_year+1900, sys_time.tm_mon+1, sys_time.tm_mday,
                sys_time.tm_hour, sys_time.tm_min, sys_time.tm_sec, millisecond);
    }
}

/************************************************************************
函数名称: get_log_level
函数功能: 根据日志等级获取对应字符串
函数参数: log_level: 日志等级
函数返回: 日志等级信息字符串
************************************************************************/
static unsigned char *get_log_level(const unsigned char log_level)
{
    switch (log_level)
    {
        case LOG_FATAL:
        {
            return "FATAL";
        }

        case LOG_ERROR:
        {
            return "ERROR";
        }

        case LOG_WARN :
        {
            return "WARN";
        }

        case LOG_INFO :
        {
            return "INFO";
        }

        case LOG_TRACE:
        {
            return "TRACE";
        }

        case LOG_DEBUG:
        {
            return "DEBUG";
        }

        case LOG_ALL:
        {
            return "ALL";
        }

        default:
        {
            return "OTHER";
        }
    }
}

/************************************************************************
函数名称: get_file_lines
函数功能: 获取指定文件的行数
函数参数: file_name: 文件名
函数返回: 成功: 返回当前文件的行数
          失败: 返回-1
************************************************************************/
static int get_file_lines(const char *file_name)
{
    FILE *fstream = NULL;
    char *cmd_buf = NULL;       // 文件路径+文件名长度
    char *out_buf = NULL;       // 命令输出结果buf(文件路径+文件名长度+文件行数的长度)
    int line = 0;      // 返回的文件行数

    // 申请内存
    cmd_buf = calloc(strlen(file_name) + 7, 1);
    out_buf = calloc(strlen(file_name) + 10, 1);

    sprintf(cmd_buf, "wc -l %s", file_name);

    if (fstream = popen(cmd_buf, "r"))      // fork子进程执行命令
    {
        memset(out_buf, 0x00, sizeof(out_buf));
        if (fgets(out_buf, sizeof(out_buf), fstream))       // 获取命令输出结果
        {
            pclose(fstream);        // 关闭popen fork的子进程
            
            line = strchr((const char *)out_buf, ' ') - out_buf;     // 获取长度, 第一次出现空格的位置
            out_buf[line] = '\0';
            line = atoi(out_buf);       // 计算文件行数
            
            free(cmd_buf);
            free(out_buf);

            return line;
        }
    }

    if (fstream)
    {
        pclose(fstream);
    }

    free(cmd_buf);
    free(out_buf);

    return -1;
}

/************************************************************************
函数名称: printf_log
函数功能: 打印日志(打印代码文件名、函数名、代码所在行)
函数参数: file_name: 代码文件名
          function_name: 代码所在函数名
          code_line: 代码行
          log_data: 日志具体内容
函数返回: 无
************************************************************************/
void printf_log(const char *file_name, const char *function_name, \
                const unsigned int code_line, const char *log_data)
{
    unsigned char get_time_str[30] = {0};      // 获取到的时间字符串

    memset(get_time_str, 0, sizeof(get_time_str));
    get_time(get_time_str, 0);

    printf("%s [%s] [%s] [%04d]: %s\n", get_time_str, file_name, function_name, code_line, log_data);
}

/************************************************************************
函数名称: write_log_file
函数功能: 将内容写到日志文件中(记录代码文件名、函数名、代码所在行)
函数参数: file_name: 代码文件名
          function_name: 代码所在函数名
          code_line: 代码行
          log_level: 日志等级
          log_data: 日志具体内容
函数返回: 无
************************************************************************/
void write_log_file(const char *file_name, const char *function_name, const unsigned int code_line, \
                    const unsigned char log_level, const char *log_data)
{
    FILE *fp = NULL;
    int ret = -1;
    static unsigned char run_num_flag = 0;      // 函数运行次数标志位
    static char log_file_name[50] = {0};        // 日志文件名(静态变量, 便于下次记录仍旧知道文件名)
    int lines = 0;         // 获取到的日志文件行数
    unsigned char get_time_str[30] = {0};      // 获取到的时间字符串
    unsigned char write_log_data[2048] = {0};   // 最终写入日志文件的内容
    char log_dir[20] = {0};     // log文件夹内的日期文件夹名
    char date_buf[15] = {0};    // 获取到的日期

    if ((NULL == file_name) || (NULL == log_data))
    {
        return;
    }

    if (log_level > LOG_LEVEL)      // 过滤日志等级
    {
        return;
    }

    ret = access(LOG_FOLDER_NAME, 0);       // 判断当前路径下是否存在log文件夹
    if (-1 == ret)
    {
        mkdir(LOG_FOLDER_NAME, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);      // 创建一个log文件夹
    }

    if (0 == run_num_flag)      // 函数是第一次运行, 需要创建一个全新的文件
    {
        get_time(get_time_str, 1);
        memset(date_buf, 0, sizeof(date_buf));
        strncpy(date_buf, get_time_str, 10);     // 获取前面的日期
        memset(log_dir, 0, sizeof(log_dir));
        sprintf(log_dir, "./%s/%s", LOG_FOLDER_NAME, date_buf);
        ret = access(log_dir, 0);       // 判断log文件夹内是否存在日期文件夹
        if (-1 == ret)
        {
            mkdir(log_dir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);      // 在log文件夹内创建一个日期文件夹
        }

        memset(log_file_name, 0, sizeof(log_file_name));
        sprintf(log_file_name, "%s/%s_%s.log", log_dir, LOG_FILE_NAME_PREFIX, get_time_str);

        run_num_flag = 1;
    }
    else
    {
        get_time(get_time_str, 1);
        memset(date_buf, 0, sizeof(date_buf));
        strncpy(date_buf, get_time_str, 10);     // 获取前面的日期
        memset(log_dir, 0, sizeof(log_dir));
        sprintf(log_dir, "./%s/%s", LOG_FOLDER_NAME, date_buf);
        ret = access(log_dir, 0);       // 判断log文件夹内是否存在正确的日期文件夹
        if (-1 == ret)
        {
            mkdir(log_dir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);      // 在log文件夹内创建一个日期文件夹

            // 更新日志文件名
            memset(log_file_name, 0, sizeof(log_file_name));
            sprintf(log_file_name, "%s/%s_%s.log", log_dir, LOG_FILE_NAME_PREFIX, get_time_str);
        }
        else        // log文件夹下面有正确的日期文件夹, 则继续判断上次的文件名行数是否超限
        {
            lines = get_file_lines(log_file_name);
            if (lines >= LOG_FILE_MAX_LINE)     // 如果文件行数超过限制, 则重新换一个文件记录日志
            {
                memset(log_file_name, 0, sizeof(log_file_name));
                sprintf(log_file_name, "%s/%s_%s.log", log_dir, LOG_FILE_NAME_PREFIX, get_time_str);
            }
        }
    }
    
    fp = fopen(log_file_name, "at+");       // 打开文件, 每次写入的时候在后面追加
    if (NULL == fp)
    {
        return;
    }

    // 写入日志时间
    memset(get_time_str, 0, sizeof(get_time_str));
    get_time(get_time_str, 0);
    fputs(get_time_str, fp);

    // 写入日志内容
    if (1 == WRITE_LOG_FLAG)        // 在日志信息中显示"文件名/函数名/代码行数"信息
    {
        snprintf(write_log_data, sizeof(write_log_data)-1, " [%s] [%s] [%04d] [%s]: %s\n", \
                 file_name, function_name, code_line, get_log_level(log_level), log_data);
    }
    else        // 不用在日志信息中显示"文件名/代码行数"信息
    {
        snprintf(write_log_data, sizeof(write_log_data)-1, "[%s]%s\n", get_log_level(log_level), log_data);
    }
    fputs(write_log_data, fp);

    fflush(fp);     // 刷新文件
    fclose(fp);     // 关闭文件
    fp = NULL;      // 将文件指针置为空
}



