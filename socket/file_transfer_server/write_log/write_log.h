/**
 * 文件名称: write_log.h
 * 摘    要: linux应用程序下记录日志头文件
 * 来    源: 参考: https://blog.csdn.net/qq_39436605/article/details/81181061
 *
 * 当前版本: 1.0 
 * 作    者: huenrong
 * 完成日期: 2019年06月20日
 **/


#ifndef __WRITE_LOG_H
#define __WRITE_LOG_H

#ifdef __cplusplus
extern "C" {
#endif

// 可修改宏定义
#define     LOG_LEVEL               6           // 打印日志等级
#define     WRITE_LOG_FLAG          1	    	// 写日志标志位, 1: 在日志信息中显示"文件名/函数名/代码行数"信息
#define     LOG_FILE_NAME_PREFIX    "file_transfer_server"      // 日志文件名前缀
#define     LOG_FILE_MAX_LINE       1000        // 日志文件最大行数
#define     LOG_FOLDER_NAME         "file_transfer_server_log"  // 日志文件夹名

// 日志级别定义
#define     LOG_FATAL               0           // 严重错误
#define     LOG_ERROR               1           // 一般错误
#define     LOG_WARN                2           // 警告
#define     LOG_INFO                3           // 一般信息
#define     LOG_TRACE               4           // 跟踪信息
#define     LOG_DEBUG               5           // 调试信息
#define     LOG_ALL                 6           // 所有信息

// 函数宏定义
#define     PRINTF_LOG(msg)                     printf_log(__FILE__, __FUNCTION__, __LINE__, msg)
#define     WRITE_LOG_FILE(level, msg)          write_log_file(__FILE__, __FUNCTION__, __LINE__, level, msg)


/************************************************************************
函数名称: printf_log
函数功能: 打印日志(打印代码文件名、函数名、代码所在行)
函数参数: file_name: 代码文件名
          function_name: 代码所在函数名
          code_line: 代码行
          log_data: 日志具体内容
函数返回: 无
************************************************************************/
void printf_log(const char *file_name, const char *function_name, \
                const unsigned int code_line, const char *log_data);

/************************************************************************
函数名称: write_log_file
函数功能: 将内容写到日志文件中(记录代码文件名、函数名、代码所在行)
函数参数: file_name: 代码文件名
          function_name: 代码所在函数名
          code_line: 代码行
          log_level: 日志等级
          log_data: 日志具体内容
函数返回: 无
************************************************************************/
void write_log_file(const char *file_name, const char *function_name, const unsigned int code_line, \
                    const unsigned char log_level, const char *log_data);


#ifdef __cplusplus
}
#endif

#endif /* __WRITE_LOG_H */

